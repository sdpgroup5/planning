from preprocessing.preprocessing import *
from postprocessing.postprocessing import *
from vision.vision import *
from planning.models import *
from communications.controller import *
from communications.vector2 import *
from planning.defender import *
from planning.attacker import *
import math
import sys
import threading

TIME_STEP = 0.1

robot_properties = {"wheel_angles" : {"LEFT" : math.pi, "RIGHT" : 0, "BACK" : 3 * (math.pi / 2)}, "wheel_radii" : {"LEFT" : 7.4, "RIGHT" : 7.4, "BACK" : 7.4}}

# input = raw_input("Enter serial port: ")
# port = "/dev/ttyACM" + input[len(input)-1:len(input)]
port = "/dev/ttyACM0"
pitch = int(raw_input("Enter pitch number: "))
colour = {"b": "blue", "y": "yellow"}[raw_input("Enter colour: ")]
our_side = {"l": "left", "r": "right"}[raw_input("Enter side: ")]

camera = Camera(port=0, pitch=pitch)

class CameraThread(threading.Thread):

    def __init__(self, camera):
        threading.Thread.__init__(self)
        self.daemon = True
        self.camera = camera
        self.current_frame = None
        self.frame_lock = threading.Lock()

    def run(self):
        while(True):
            self.current_frame = self.camera.get_frame()

    def get_current_frame(self):
        frame = self.current_frame
        return frame

# class PlanningThread(threading.Thread):

#     def __init__(self):
#         threading.Thread.__init__(self)
#         self.daemon = True

#     def run(self):

frame = camera.get_frame()
frame_shape = frame.shape
frame_centre = camera.get_adjusted_center(frame)

calibration = tools.get_colors(pitch)

camera_thread = CameraThread(camera)
camera_thread.start()

communications = Controller(port, robot_properties, 0.5)

preprocessing = Preprocessing()
postprocessing = Postprocessing()
vision = Vision(pitch, colour, our_side, frame_shape, frame_centre, calibration)
zone = 0 if our_side == "left" else 3
defender = Defender(preprocessing, postprocessing, vision, camera, camera_thread.get_current_frame, communications, our_side, pitch)
attacker = Attacker(preprocessing, postprocessing, vision, camera_thread.get_current_frame, communications, our_side, pitch, zone)

while True:
    condition = True
    defender.updatepositions()
    attacker.updatepositions()
    ball = attacker.world.ball
    zones = attacker.world.pitch.zones
    cur_zone = 3

    v_ball_pos = Vector2(ball.x, ball.y)
    for idx in range(0,4):
        if (zones[idx].isInside(ball.x, ball.y)):
            cur_zone = idx
            break
    if (ball.x < 1 and ball.y < 1):
        cur_zone = 3

    if cur_zone == zone:
        attacker.do_logic()
    else:
        defender.intercept()
    time.sleep(TIME_STEP)
