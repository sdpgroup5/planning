from utilities import *
import math
from random import randint
from communications.vector2 import Vector2
from communications.command import KickCommand, GrabCommand, StopCommand, \
        MoveCommand

from models import *
from datetime import datetime,timedelta

LOG_STRT = 1

# Global variables
PIXEL_TO_CM = 115.2 / 280.0
CM_TO_PIXEL = 1.0 / PIXEL_TO_CM
GRAB_RANGE = 20 * CM_TO_PIXEL
LOSE_RANGE = 40 * CM_TO_PIXEL
MAX_ANG_VEL = 4.5
MAX_VEL = 70
SHOOTING_X_OFFSET = 85
GOAL_CORNER_OFFSET = 55
ROT_ANGLE_THRES  = 0.15 # 0.15 radians = 8.6 degrees
GOAL_ANGLE_THRES = 0.15 # 0.15 radians = 8.6 degrees

class Strategy(object):

    PRECISE_BALL_ANGLE_THRESHOLD = math.pi / 15.0
    UP, DOWN = 'UP', 'DOWN'

    def __init__(self, world, states, communications):
        self.world = world
        self.states = states
        self._current_state = states[0]
        self.communications = communications

    @property
    def current_state(self):
        return self._current_state

    @current_state.setter
    def current_state(self, new_state):
        assert new_state in self.states
        self._current_state = new_state

    def reset_current_state(self):
        self.current_state = self.states[0]

    def is_last_state(self):
        return self._current_state == self.states[-1]

    def generate(self):
        return self.NEXT_ACTION_MAP[self.current_state]()

    def rotate_to_angle(self, desired_angle, travel_power = 0.5, precision_power = 0.5):
        # --- VELOCITY COMPONENT ---
        # How much we're offset from the ideal angle
        offset_from_desired_angle = (self.our_attacker.current.angle - desired_angle) % (math.pi * 2)
        if offset_from_desired_angle > math.pi:
            offset_from_desired_angle -= math.pi * 2

        precision_range = math.pi / 3
        desired_ang_vel = math.copysign(travel_power, -offset_from_desired_angle)
        if math.fabs(offset_from_desired_angle) < precision_range:
            desired_ang_vel = -offset_from_desired_angle / precision_range * precision_power

        # --- ACCELERATION COMPONENT ---
        # Smoothing and cm/s -> power conversion
        cur_ang_vel = ((self.our_attacker.current.ang_vel + self.our_attacker.last.ang_vel) / 2) / MAX_ANG_VEL

        # Simulated acceleration/braking
        acceleration_compensation = (desired_ang_vel - cur_ang_vel) * 0.25
        power = desired_ang_vel + acceleration_compensation

        return power

    def go_to_position(self, desired_position, travel_power = 1, precision_power = 0.5):
        # --- VELOCITY COMPONENT ---
        displacement = desired_position - self.our_attacker.current.position

        # Range in which we attempt precise positioning
        precision_range = 16.5 * CM_TO_PIXEL
        precision_scaling = 0.5
        desired_power = displacement.norm() * travel_power
        if displacement.len() < precision_range:
            desired_power = displacement / precision_range * precision_power

        # --- ACCELERATION COMPONENT ---
        # Smoothing
        current_power = (self.our_attacker.current.velocity + self.our_attacker.last.velocity) / 2 / MAX_VEL

        # Add acceleration to desired velocity
        acceleration_compensation = (desired_power - current_power) / 4
        power = desired_power + acceleration_compensation
        return power

    def idle(self):
        pass


class AttackerIntercept(Strategy):
    '''
    Open the catcher if closed and chase the ball on the y axis
    '''
    INTERCEPT = 'INTERCEPT'
    STATES = [INTERCEPT]

    def __init__(self, world, communications):
        super(AttackerIntercept, self).__init__(world, self.STATES,
                        communications)

        self.NEXT_ACTION_MAP = {
            self.INTERCEPT : self.intercept
        }

        self.our_attacker = self.world.our_attacker
        self.ball = self.world.ball
        zone       = 2 if self.world._our_side == 'left' else 1
        their_zone = 1 if self.world._our_side == 'left' else 2
        box = self.world.pitch.zones[zone].boundingBox()
        their_box = self.world.pitch.zones[their_zone].boundingBox()
        self.zone_box = {
                "min": Vector2(box[0], box[2]),
                "max": Vector2(box[1], box[3])}
        self.their_zone_box = {
            "min": Vector2(their_box[0], their_box[2]),
            "max": Vector2(their_box[1], their_box[3])}

    def intercept(self):
        '''
        Open the catcher if it is closed or do nothing if open.
        Set state to catch to proceed with the next function.
        '''

        zone_center = (self.zone_box["max"] + self.zone_box["min"]) * 0.5
        pi       = math.pi
        our_side = self.world._our_side
        ball_ang = self.ball.current.angle
        ball_vel = self.ball.current.velocity.len()
        ball     = self.ball.current.position
        att      = self.our_attacker.current.position

        # Straight intercept calculation
        y = math.tan(ball_ang) * (zone_center.x - ball.x) + ball.y

        # Even if the target is out of the pitch, we should stay in the pitch
        if y > self.zone_box['max'].y:
            y = self.zone_box['max'].y
        if y < self.zone_box['min'].y:
            y = self.zone_box['min'].y

        if LOG_STRT > 4:
            print 'ball angle: ', ball_ang / pi, ' pi'

        # If the ball is moving in the opposite way from us, go to its current
        # position instead of its planned position
        if (our_side == 'left'  and      0.5 * pi <= ball_ang <= 1.5 * pi)  or \
           (our_side == 'right' and not  0.5 * pi <= ball_ang <= 1.5 * pi):
            if LOG_STRT > 2:
                print 'go to current ball position, wrong way'
            y = ball.y
        # bounce intercept
        # c can be negative - that means that it is not bouncing intercept
        elif our_side == 'left' and 1.5 * pi < ball_ang < 2.0 * pi:
            alpha = 2.0 * pi - ball_ang
            a = self.their_zone_box['max'].y - ball.y
            b = a / math.tan(alpha)
            c = zone_center.x - b - ball.x
            if c > 0:
                y = c * math.tan(alpha)
                if LOG_STRT > 1:
                    print 'bounce intercept in right down direction'
        elif our_side == 'left' and 0.0 < ball_ang < 0.5 * pi:
            alpha = ball_ang
            a = ball.y - self.their_zone_box['min'].y
            b = a / math.tan(alpha)
            c = zone_center.x - b - ball.x
            if c > 0:
                y = c * math.tan(alpha)
                if LOG_STRT > 1:
                    print 'bounce intercept in right up direction'
        elif our_side == 'right' and pi < ball_ang < 1.5 * pi:
            alpha = ball_ang - pi
            a = self.their_zone_box['max'].y - ball.y
            b = a / math.tan(alpha)
            c = ball.x - b - zone_center.x
            if c > 0:
                y = c * math.tan(alpha)
                if LOG_STRT > 1:
                    print 'bounce intercept in left down direction'
        elif our_side == 'right' and 0.5 * pi < ball_ang < pi:
            alpha = pi - ball_ang
            a = ball.y - self.their_zone_box['min'].y
            b = a / math.tan(alpha)
            c = ball.x - b - zone_center.x
            if c > 0:
                y = c * math.tan(alpha)
                if LOG_STRT > 1:
                    print 'bounce intercept in left up direction'

        # Even if the target is out of the pitch, we should stay in the pitch
        # Calculating for second time because the value y can be changed by
        # the bouncing intercept cases
        if y > self.zone_box['max'].y:
            y = self.zone_box['max'].y
        if y < self.zone_box['min'].y:
            y = self.zone_box['min'].y

        # if time for ball to reach position is more than 4 seconds, it means
        # that it is moving too slow
        # s / v = t; s > t * v
        if math.fabs(y - att.y) > math.fabs(4.0 * ball_vel):
            if LOG_STRT > 2:
                print 'go to current ball position, too slow'
            y = ball.y
        # if time is between 4 and 5 seconds and we are between the
        # current and the planned ball position, closer to the current,
        # move to the middle of the segment between current and planned
        elif math.fabs(y - att.y) > math.fabs(3.0 * ball_vel) and \
            (ball.y < att.y < (y + ball.y) / 2 or \
             (y + ball.y) / 2 < att.y < ball.y):
            if LOG_STRT > 2:
                print 'go halfway between current and predicted ball position'
            y = (y + ball.y) / 2


        desired_position = Vector2(zone_center.x, y)
        displacement = desired_position - self.our_attacker.current.position

        lin_power = self.go_to_position(desired_position, travel_power = 0.6,
                precision_power = 0.3)

        intercept_line = self.ball.current.position - \
                         self.our_attacker.current.position
        desired_angle = intercept_line.angle()

        ang_power = self.rotate_to_angle(desired_angle, travel_power = 0.4, \
                                         precision_power = 0.3)
        offset_from_desired_angle = \
            (self.our_attacker.current.angle - desired_angle) % (math.pi * 2)
        if offset_from_desired_angle > math.pi:
            offset_from_desired_angle -= math.pi * 2

        if displacement.len() < 10.0 * CM_TO_PIXEL:
            lin_power = Vector2(0,0)

        if math.fabs(offset_from_desired_angle) < 0.2:
            ang_power = 0

        command = MoveCommand(power = lin_power, angle = self.our_attacker.angle,
                rotation_power = ang_power)
        self.communications.put_command(command)


class AttackerPosition(Strategy):

    OPEN, GET_TO_POSITION = 'OPEN', 'GET_IN_POSITION'
    STATES = [OPEN, GET_TO_POSITION]

    def __init__(self, world, communications):
        super(AttackerPosition, self).__init__(world, self.STATES,
                communications)

        self.NEXT_ACTION_MAP = {self.OPEN : self.open_kick,
                                self.GET_TO_POSITION : self.move}

        self.our_attacker = self.world.our_attacker
        self.our_defender = self.world.our_defender
        self.their_attacker = self.world.their_attacker
        self.ball = self.world.ball
        zone = 2 if self.world._our_side == 'left' else 1
        box = self.world.pitch.zones[zone].boundingBox()
        self.zone_box = {
                "min": Vector2(box[0], box[2]),
                "max": Vector2(box[1], box[3])}

    def open_kick(self):
        self.communications.put_command(KickCommand())
        self.current_state = self.GET_TO_POSITION

    def move(self):
        zone_center = (self.zone_box["max"] + self.zone_box["min"]) * 0.5
        desired_position = Vector2(zone_center.x,self.our_defender.current.position.y)
        displacement = self.our_attacker.current.position - desired_position

        if displacement.len() < 10.0 * CM_TO_PIXEL:
            linear_power = Vector2(0,0)
        else:
            linear_power = self.go_to_position(desired_position, travel_power = 0.6,
                    precision_power = 0.4)

        shooting_line = self.our_defender.current.position - self.our_attacker.current.position
        desired_angle = shooting_line.angle()
        ang_power = self.rotate_to_angle(desired_angle, travel_power = 0.4, precision_power = 0.3)
        offset_from_desired_angle = (self.our_attacker.current.angle - desired_angle) % (math.pi * 2)

        if offset_from_desired_angle > math.pi:
            offset_from_desired_angle -= math.pi * 2

        command = MoveCommand(power = linear_power,
                            angle = self.our_attacker.angle,
                            rotation_power = ang_power)

        self.communications.put_command(command)


class AttackerGrab(Strategy):
    '''
    Open the catcher if closed, chase and grab the ball
    '''
    PREPARE, MOVE_AWAY, GO_TO_BALL, GRAB_BALL, GRABBED = 'PREPARE', 'MOVE_AWAY', 'GO_TO_BALL', \
                                              'GRAB_BALL', 'GRABBED'
    STATES = [PREPARE, MOVE_AWAY, GO_TO_BALL, GRAB_BALL, GRABBED]

    def __init__(self, world, communications):
        super(AttackerGrab, self).__init__(world, self.STATES, communications)

        self.NEXT_ACTION_MAP = {
            self.PREPARE: self.prepare,
            self.MOVE_AWAY: self.move_away,
            self.GO_TO_BALL: self.go_to_ball,
            self.GRAB_BALL: self.grab,
            self.GRABBED: self.idle
        }

        self.our_attacker = self.world.our_attacker
        self.ball = self.world.ball

    def prepare(self):
        '''
        Open the catcher.
        '''
        self.communications.put_command(KickCommand())
        self.current_state = self.GO_TO_BALL

    def move_away(self):
        '''
        Move away from the ball in order not to nudge it.
        '''
        proximity_distance = 17 * CM_TO_PIXEL
        ball_pos = self.ball.current.position
        robot_pos = self.our_attacker.current.position
        grab_line = ball_pos - robot_pos
        grab_line_inverted = Vector2(0,0)
        if LOG_STRT > 1:
            print 'MOVE AWAY'

        if(grab_line.len() < proximity_distance):
          if LOG_STRT > 1:
            print grab_line.len();
          grab_line_norm = grab_line.norm()
          grab_line_inverted.x = -(grab_line_norm.x)
          grab_line_inverted.y = -(grab_line_norm.y)
          ideal_position = (robot_pos + grab_line_inverted * proximity_distance)
          lin_power = self.go_to_position(ideal_position, travel_power = 0.6,
                    precision_power = 0.4)

          ang_power = self.rotate_to_angle(0, travel_power = 0.0, \
          precision_power = 0.0)

          command = MoveCommand(power = lin_power,
                      angle = self.our_attacker.angle,
                      rotation_power = ang_power)
          self.communications.put_command(command)

          ball_pos = self.ball.current.position
          robot_pos = self.our_attacker.current.position
          grab_line = ball_pos - robot_pos
        else:
          self.communications.put_command(StopCommand())
          self.current_state = self.GO_TO_BALL

    def go_to_ball(self):
        '''
        Chase the ball.
        Set state to grab_ball to proceed with the next function.
        '''
        ball_pos = self.ball.current.position
        robot = self.our_attacker
        robot_pos = self.our_attacker.current.position
        grab_line = ball_pos - robot_pos
        ideal_angle = grab_line.angle()

        proximity_radius = 16 * CM_TO_PIXEL

        proximity = grab_line.len() < proximity_radius

        x = ball_pos.x
        y = ball_pos.y

        back_left   = (-0.5 * CM_TO_PIXEL,  -3.25 * CM_TO_PIXEL)
        back_right  = (-0.5 * CM_TO_PIXEL,   3.25 * CM_TO_PIXEL)
        front_left  = (4    * CM_TO_PIXEL,  -9.5 * CM_TO_PIXEL)
        front_right = (4    * CM_TO_PIXEL,   9.5 * CM_TO_PIXEL)
        buffer = Polygon((front_left, front_right, back_right, back_left))
        buffer.shift(robot.x + robot._catcher_area['front_offset'], robot.y)
        buffer.rotate(robot.angle, robot.x, robot.y)

        in_buffer = buffer.isInside(ball_pos.x, ball_pos.y)

        back_left   = (0,                 -2.75 * CM_TO_PIXEL)
        back_right  = (0,                  2.75 * CM_TO_PIXEL)
        front_left  = (3.5 * CM_TO_PIXEL, -9.0 * CM_TO_PIXEL)
        front_right = (3.5 * CM_TO_PIXEL,  9.0 * CM_TO_PIXEL)
        catcher = Polygon((front_left, front_right, back_right, back_left))
        catcher.shift(robot.x + robot._catcher_area['front_offset'], robot.y)
        catcher.rotate(robot.angle, robot.x, robot.y)

        in_catcher = catcher.isInside(ball_pos.x, ball_pos.y)


        A = Vector2(back_right[0], back_right[1])
        B = Vector2(front_right[0], front_right[1])

        C = Vector2(back_left[0], back_left[1])
        D = Vector2(front_left[0], front_left[1])

        A = A + robot_pos + Vector2(robot._catcher_area['front_offset'], 0)
        B = B + robot_pos + Vector2(robot._catcher_area['front_offset'], 0)
        C = C + robot_pos + Vector2(robot._catcher_area['front_offset'], 0)
        D = D + robot_pos + Vector2(robot._catcher_area['front_offset'], 0)

        A = A.rotate_pivot(robot_pos, robot.angle)
        B = B.rotate_pivot(robot_pos, robot.angle)
        C = C.rotate_pivot(robot_pos, robot.angle)
        D = D.rotate_pivot(robot_pos, robot.angle)

        above_AB = (x - A.x)*(B.y - A.y) - (y - A.y)*(B.x - A.x) > 0
        #if LOG_STRT > 1:
            #print 'AB: ', (x - A.x)*(B.y - A.y) - (y - A.y)*(B.x - A.x)
        below_CD = (x - C.x)*(D.y - C.y) - (y - C.y)*(D.x - C.x) < 0
        #if LOG_STRT > 1:
            #print 'CD: ', (x - C.x)*(D.y - C.y) - (y - C.y)*(D.x - C.x)

        if proximity and not (above_AB and below_CD) :
            self.current_state = self.MOVE_AWAY
            #pass

        elif not in_catcher:

            #equilibrium_distance = 20 * CM_TO_PIXEL
            ideal_position = ball_pos

            if in_buffer:
                lin_power = self.go_to_position(ideal_position, travel_power = 0.4,
                    precision_power = 0.25)
                ang_power = self.rotate_to_angle(ideal_angle, travel_power = 0.4, \
                precision_power = 0.25)
            else:
                lin_power = self.go_to_position(ideal_position, travel_power = 0.4,
                    precision_power = 0.3)
                ang_power = self.rotate_to_angle(ideal_angle, travel_power = 0.4, \
                precision_power = 0.3)

            command = MoveCommand(power = lin_power,
                        angle = self.our_attacker.angle,
                        rotation_power = ang_power)
            self.communications.put_command(command)

        else:
            self.communications.put_command(StopCommand())
            self.current_state = self.GRAB_BALL

    def grab(self):
        '''
        Grab the ball
        '''
        self.communications.put_command(GrabCommand())
        self.current_state = self.GRABBED

class AttackerGrabCarefully(Strategy):
    '''
    Open the catcher if closed, chase and grab the ball
    '''
    PREPARE, ALIGN, GO_TO_BALL, GRAB_BALL, GRABBED = 'PREPARE', 'ALIGN', 'GO_TO_BALL', \
                                                     'GRAB_BALL', 'GRABBED'
    STATES = [PREPARE, ALIGN, GO_TO_BALL, GRAB_BALL, GRABBED]

    def __init__(self, world, communications):
        super(AttackerGrabCarefully, self).__init__(world, self.STATES, communications)

        self.NEXT_ACTION_MAP = {
            self.PREPARE: self.prepare,
            self.ALIGN: self.align,
            self.GO_TO_BALL: self.go_to_ball,
            self.GRAB_BALL: self.grab,
            self.GRABBED: self.idle
        }

        self.our_attacker = self.world.our_attacker
        self.ball = self.world.ball

    def prepare(self):
        '''
        Open the catcher.
        '''
        self.communications.put_command(KickCommand())
        self.current_state = self.ALIGN

    def align(self):
        '''
        Align with the ball.
        '''
        self.zone = 2 if self.world._our_side == 'left' else 1
        box = self.world.pitch.zones[self.zone].boundingBox()
        self.zone_box = {
                "min": Vector2(box[0], box[2]),
                "max": Vector2(box[1], box[3])}
        zone_center = (self.zone_box["max"] + self.zone_box["min"]) * 0.5
        ball_pos = self.ball.current.position
        robot_pos = self.our_attacker.current.position
        tolerance_distance = 5 * CM_TO_PIXEL

        if(abs(ball_pos.y - robot_pos.y) > tolerance_distance):

          ideal_position = Vector2(zone_center.x, ball_pos.y)

          if(ball_pos.x > zone_center.x):
            ideal_angle = 0
          else:
            ideal_angle = math.pi

          lin_power = self.go_to_position(ideal_position, travel_power = 0.5,
                    precision_power = 0.3)

          ang_power = self.rotate_to_angle(ideal_angle, travel_power = 0.3, \
          precision_power = 0.25)

          command = MoveCommand(power = lin_power,
                      angle = self.our_attacker.angle,
                      rotation_power = ang_power)
          self.communications.put_command(command)

        else:
          self.communications.put_command(StopCommand())
          self.current_state = self.GO_TO_BALL

    def go_to_ball(self):
        '''
        Chase the ball.
        Set state to grab_ball to proceed with the next function.
        '''
        ball_pos = self.ball.current.position
        robot = self.our_attacker
        robot_pos = self.our_attacker.current.position
        grab_line = ball_pos - robot_pos
        ideal_angle = grab_line.angle()

        proximity_radius = 16 * CM_TO_PIXEL

        proximity = grab_line.len() < proximity_radius

        x = ball_pos.x
        y = ball_pos.y

        back_left   = (-0.5 * CM_TO_PIXEL,  -3.25 * CM_TO_PIXEL)
        back_right  = (-0.5 * CM_TO_PIXEL,   3.25 * CM_TO_PIXEL)
        front_left  = (4    * CM_TO_PIXEL,  -9.5 * CM_TO_PIXEL)
        front_right = (4    * CM_TO_PIXEL,   9.5 * CM_TO_PIXEL)
        buffer = Polygon((front_left, front_right, back_right, back_left))
        buffer.shift(robot.x + robot._catcher_area['front_offset'], robot.y)
        buffer.rotate(robot.angle, robot.x, robot.y)

        in_buffer = buffer.isInside(ball_pos.x, ball_pos.y)

        back_left   = (0,                 -2.75 * CM_TO_PIXEL)
        back_right  = (0,                  2.75 * CM_TO_PIXEL)
        front_left  = (3.5 * CM_TO_PIXEL, -9.0 * CM_TO_PIXEL)
        front_right = (3.5 * CM_TO_PIXEL,  9.0 * CM_TO_PIXEL)
        catcher = Polygon((front_left, front_right, back_right, back_left))
        catcher.shift(robot.x + robot._catcher_area['front_offset'], robot.y)
        catcher.rotate(robot.angle, robot.x, robot.y)

        in_catcher = catcher.isInside(ball_pos.x, ball_pos.y)

        tolerance_distance = 5 * CM_TO_PIXEL

        if(abs(ball_pos.y - robot_pos.y) > tolerance_distance):
            self.current_state = self.ALIGN

        elif not in_catcher:

            ideal_position = ball_pos

            lin_power = self.go_to_position(ideal_position, travel_power = 0.3,
                precision_power = 0.25)
            ang_power = self.rotate_to_angle(ideal_angle, travel_power = 0.3, \
            precision_power = 0.25)


            command = MoveCommand(power = lin_power,
                        angle = self.our_attacker.angle,
                        rotation_power = ang_power)
            self.communications.put_command(command)

        else:
            self.communications.put_command(StopCommand())
            self.current_state = self.GRAB_BALL

    def grab(self):
        '''
        Grab the ball
        '''
        self.communications.put_command(GrabCommand())
        self.current_state = self.GRABBED

class AttackerShoot(Strategy):

    POSITIONING, SHOOTING, IDLE = 'POSITIONING', 'SHOOTING', 'IDLE'
    STATES = [POSITIONING, SHOOTING, IDLE]

    UP, DOWN = 'UP', 'DOWN'
    GOAL_SIDES = [UP, DOWN]

    SHOOTING_X_OFFSET = 85
    GOAL_CORNER_OFFSET = 55

    def __init__(self, world, communications):
        super(AttackerShoot, self).__init__(world, self.STATES,
                communications)

        self.NEXT_ACTION_MAP = {self.POSITIONING : self.position,
                                self.SHOOTING : self.shoot,
                                self.IDLE : self.idle}

        self.our_attacker = self.world.our_attacker
        self.their_defender = self.world.their_defender
        self.their_goal = self.world.their_goal
        self.zone = 2 if self.world._our_side == 'left' else 1
        box = self.world.pitch.zones[self.zone].boundingBox()
        self.zone_box = {
                "min": Vector2(box[0], box[2]),
                "max": Vector2(box[1], box[3])}

    def position(self):

        zone_center = (self.zone_box["max"] + self.zone_box["min"]) * 0.5
        #blocker_in_bottom_half = self.their_attacker.y <= zone_center.y
        desired_position = zone_center
        displacement = self.our_attacker.current.position - zone_center

        if displacement.len() < 10.0 * CM_TO_PIXEL:
            linear_power = Vector2(0,0)
        else:
            linear_power = self.go_to_position(zone_center, travel_power = 0.6,
                    precision_power = 0.3)

        if self.their_defender.y < zone_center.y:
            side = self.UP
        else:
            side = self.DOWN

        goal_y_coordinate = self._get_goal_corner_y(side)

        point = Vector2(self.world.their_goal.x, goal_y_coordinate)

        shooting_line = point - self.our_attacker.current.position
        desired_angle = shooting_line.angle()
        ang_power = self.rotate_to_angle(desired_angle, travel_power = 0.6, precision_power = 0.3)
        offset_from_desired_angle = (self.our_attacker.current.angle - desired_angle) % (math.pi * 2)

        if offset_from_desired_angle > math.pi:
            offset_from_desired_angle -= math.pi * 2

        shoot = False
        if LOG_STRT > 1:
            print offset_from_desired_angle
        if math.fabs(offset_from_desired_angle) < 0.05:
            shoot = True
            ang_power = 0

        command = MoveCommand(power = linear_power,
                            angle = self.our_attacker.angle,
                            rotation_power = ang_power)

        self.communications.put_command(command)

        if (displacement.len() < 10.0 * CM_TO_PIXEL and math.fabs(offset_from_desired_angle) < GOAL_ANGLE_THRES):
            if shoot == True:
                self.communications.put_command(StopCommand())
                self.current_state = self.SHOOTING

    def shoot(self):
        self.communications.put_command(KickCommand())
        self.current_state = self.IDLE

    def _get_goal_corner_y(self, side):
        """
        Get the coordinates of where to aim / shoot.
        """
        assert side in self.GOAL_SIDES
        if side == self.UP:
            # y coordinate of the goal is DOWN, offset by the width
            return (self.world.their_goal.y + self.world.their_goal.width / 2) \
                   - 10 * CM_TO_PIXEL
        return (self.world.their_goal.y - self.world.their_goal.width / 2) \
               + 10 * CM_TO_PIXEL


class AttackerDefend(Strategy):

    KICK, BLOCKING = 'KICK', 'BLOCKING'
    STATES = [KICK, BLOCKING]

    def __init__(self, world, communications):
        super(AttackerDefend, self).__init__(world, self.STATES, communications)

        self.NEXT_ACTION_MAP = {
            self.BLOCKING: self.block,
            self.KICK: self.kick}

        self.zone = self.world._pitch._zones[self.world.our_attacker.zone]
        min_x, max_x, self.min_y, self.max_y  = self.zone.boundingBox()
        self.center_x = (min_x + max_x)/2
        self.center_y = (self.min_y + self.max_y)/2
        self.zone_center = Vector2(self.center_x,self.center_y)
        self.our_attacker = self.world.our_attacker
        self.their_defender = self.world.their_defender

    def kick(self):
        self.communications.put_command(KickCommand())
        self.current_state = self.BLOCKING

    def block(self):
        upper_angle = 0
        lower_angle = 0
        if (self.world.our_attacker.zone == 2):
            #upper_angle = 2.6
            #lower_angle = 4.6
            upper_angle = (3.0/2)*math.pi-0.01
            lower_angle = math.pi/2 + 0.01
        else:
            #upper_angle = 1.0
            #lower_angle = -1.0
            upper_angle = math.pi/2 - 0.01
            lower_angle = -math.pi/2 + 0.01

        def_angle = self.their_defender.angle
        # calculate the equation of the line the defending robot is facing
        grad = math.tan(def_angle)
        x = self.their_defender.x
        y = self.their_defender.y
        constant = y - (x*grad)

        # find the predicted point of intersection
        predicted_y = (self.center_x*grad) + constant
        bound = 25

        if (predicted_y > self.max_y - bound):
            predicted_y = self.max_y - bound
        elif (predicted_y < self.min_y + bound):
            predicted_y = self.min_y + bound

        predicted_vector = Vector2(self.center_x, predicted_y)

        linear_power = self.go_to_position(predicted_vector, travel_power = 0.8,
                precision_power = 0.5)

        displacement = self.our_attacker.current.position - predicted_vector

        # rotate to face opposing defender
        blocking_line = self.their_defender.current.position - self.our_attacker.current.position
        desired_angle = blocking_line.angle()

        ang_power = self.rotate_to_angle(desired_angle, travel_power = 0.5, precision_power = 0.3)
        offset_from_desired_angle = (self.our_attacker.current.angle - desired_angle) % (math.pi * 2)

        if offset_from_desired_angle > math.pi:
            offset_from_desired_angle -= math.pi * 2

        if not (math.fabs(offset_from_desired_angle) > 0.3):
            ang_power = 0

        command = MoveCommand(power = linear_power, angle = self.our_attacker.angle, rotation_power = ang_power)

        if (self.world.our_attacker.zone == 1 and def_angle > math.pi):
            def_angle -= math.pi*2


        if (displacement.len() < 10 * CM_TO_PIXEL):
            command = StopCommand()
            self.communications.put_command(command)
        elif (def_angle < upper_angle and def_angle > lower_angle):
            self.communications.put_command(command)
        else:
            linear_power = self.go_to_position(self.zone_center, travel_power = 0.6,
                precision_power = 0.3)
            command = MoveCommand(power = linear_power, angle = self.our_attacker.angle, rotation_power = ang_power)
            self.communications.put_command(command)




class AttackerShootSlide(Strategy):

    POSITIONCENTRE, MOVESIDE, ROTATE, SHOOTING, IDLE, TIMEOUTKICK = 'POSITIONCENTRE','MOVESIDE','ROTATE','SHOOTING','IDLE', 'TIMEOUTKICK'
    STATES = [POSITIONCENTRE, MOVESIDE, ROTATE, SHOOTING, IDLE, TIMEOUTKICK]

    UP, DOWN = 'UP', 'DOWN'
    GOAL_SIDES = [UP, DOWN]

    def __init__(self, world, communications):
        super(AttackerShootSlide, self).__init__(world, self.STATES,
                communications)

        self.NEXT_ACTION_MAP = {self.POSITIONCENTRE : self.positionCentre,
                                self.MOVESIDE: self.moveSide,
                                self.ROTATE: self.rotate,
                                self.SHOOTING : self.shoot,
                                self.IDLE : self.idle,
                                self.TIMEOUTKICK : self.timeoutKick}

        self.our_attacker = self.world.our_attacker
        self.their_defender = self.world.their_defender
        self.their_goal = self.world.their_goal
        self.zone = 2 if self.world._our_side == 'left' else 1
        box = self.world.pitch.zones[self.zone].boundingBox()
        self.zone_box = {
                "min": Vector2(box[0], box[2]),
                "max": Vector2(box[1], box[3])}
        self.timer = 0
        self.last_offset = 0

    def positionCentre(self):
        zone_center = (self.zone_box["max"] + self.zone_box["min"]) * 0.5
        desired_position = zone_center
        displacement = self.our_attacker.current.position - desired_position

        if displacement.len() < 10.0 * CM_TO_PIXEL:
            linear_power = Vector2(0,0)
        else:
            linear_power = self.go_to_position(desired_position, travel_power = 0.6,
                    precision_power = 0.3)

        shooting_line = Vector2(self.their_goal.x,self.their_goal.y) - self.our_attacker.current.position
        desired_angle = shooting_line.angle()
        ang_power = self.rotate_to_angle(desired_angle, travel_power = 0.4, precision_power = 0.3)
        offset_from_desired_angle = (self.our_attacker.current.angle - desired_angle) % (math.pi * 2)

        if offset_from_desired_angle > math.pi:
            offset_from_desired_angle -= math.pi * 2

        command = MoveCommand(power = linear_power,
                            angle = self.our_attacker.angle,
                            rotation_power = ang_power)

        self.communications.put_command(command)

        if (displacement.len() < 10.0 * CM_TO_PIXEL and math.fabs(offset_from_desired_angle) < 0.25):
            self.communications.put_command(StopCommand())
            self.timer = datetime.now()
            self.current_state = self.MOVESIDE

    def moveSide(self):

        zone_center = (self.zone_box["max"] + self.zone_box["min"]) * 0.5
        moveOffset = 0
        shootOffset = 0
        if self.their_defender.y < zone_center.y:
            side = self.UP
            moveOffset = 80
            shootOffset = 60
        else:
            side = self.DOWN
            moveOffset = -80
            shootOffset = -60

        desired_position = Vector2(zone_center.x,self.their_goal.y+moveOffset)
        displacement = self.our_attacker.current.position - desired_position

        if displacement.len() < 10.0 * CM_TO_PIXEL:
            linear_power = Vector2(0,0)
        else:
            linear_power = self.go_to_position(desired_position, travel_power = 0.8,
                    precision_power = 0.3)

        shooting_line = Vector2(self.their_goal.x,self.their_goal.y+shootOffset) - self.our_attacker.current.position
        desired_angle = shooting_line.angle()
        ang_power = self.rotate_to_angle(desired_angle, travel_power = 0.6, precision_power = 0.4)
        offset_from_desired_angle = (self.our_attacker.current.angle - desired_angle) % (math.pi * 2)

        if offset_from_desired_angle > math.pi:
            offset_from_desired_angle -= math.pi * 2

        command = MoveCommand(power = linear_power,
                            angle = self.our_attacker.angle,
                            rotation_power = ang_power)

        self.communications.put_command(command)

        if (datetime.now()-self.timer > timedelta(seconds=12)):
            self.current_state = self.TIMEOUTKICK

        if (displacement.len() < 10.0 * CM_TO_PIXEL and math.fabs(offset_from_desired_angle) < 0.15):
            if (side == self.UP and self.their_defender.y < zone_center.y):
                self.communications.put_command(StopCommand())
                self.current_state = self.ROTATE
            elif (side == self.DOWN and self.their_defender.y > zone_center.y):
                self.communications.put_command(StopCommand())
                self.current_state = self.ROTATE

    def rotate(self):

        zone_center = (self.zone_box["max"] + self.zone_box["min"]) * 0.5
        shootOffset = 0
        if self.their_defender.y < zone_center.y:
            shootOffset = 50
        else:
            shootOffset = -50

        shooting_line = Vector2(self.their_goal.x,self.their_goal.y+shootOffset) - self.our_attacker.current.position
        desired_angle = shooting_line.angle()
        ang_power = self.rotate_to_angle(desired_angle, travel_power = 0.6, precision_power = 0.4)
        offset_from_desired_angle = (self.our_attacker.current.angle - desired_angle) % (math.pi * 2)

        if offset_from_desired_angle > math.pi:
            offset_from_desired_angle -= math.pi * 2

        if LOG_STRT > 1:
            print 'shoot offset: ', shootOffset
            print 'angle offset: ', offset_from_desired_angle

        command = MoveCommand(power = Vector2(0,0),
                            angle = self.our_attacker.angle,
                            rotation_power = ang_power)

        self.communications.put_command(command)

        if (math.fabs(offset_from_desired_angle) < 0.1):
            if (shootOffset > 0 and self.last_offset > 0.0):
                self.current_state = self.SHOOTING
            elif (shootOffset < 0 and self.last_offset < 0.0):
                self.current_state = self.SHOOTING
        self.last_offset = offset_from_desired_angle

    def timeoutKick(self):

        zone_center = (self.zone_box["max"] + self.zone_box["min"]) * 0.5
        moveOffset = 80
        shootOffset = -60

        desired_position = Vector2(zone_center.x,self.their_goal.y+moveOffset)
        displacement = self.our_attacker.current.position - desired_position

        if displacement.len() < 10.0 * CM_TO_PIXEL:
            linear_power = Vector2(0,0)
        else:
            linear_power = self.go_to_position(desired_position, travel_power = 0.6,
                    precision_power = 0.3)

        shooting_line = Vector2(self.their_goal.x,self.their_goal.y+shootOffset) - self.our_attacker.current.position
        desired_angle = shooting_line.angle()
        ang_power = self.rotate_to_angle(desired_angle, travel_power = 0.6, precision_power = 0.3)
        offset_from_desired_angle = (self.our_attacker.current.angle - desired_angle) % (math.pi * 2)

        if offset_from_desired_angle > math.pi:
            offset_from_desired_angle -= math.pi * 2

        command = MoveCommand(power = linear_power,
                            angle = self.our_attacker.angle,
                            rotation_power = ang_power)

        self.communications.put_command(command)

        if (displacement.len() < 10.0 * CM_TO_PIXEL and math.fabs(offset_from_desired_angle) < 0.1):
            self.communications.put_command(StopCommand())
            self.current_state = self.SHOOTING

    def shoot(self):
        self.communications.put_command(KickCommand())
        self.current_state = self.IDLE

class AttackerShootYordan(Strategy):

    POSITIONCENTRE, MOVESIDE, ROTATE, SHOOTING, IDLE, TIMEOUTKICK = 'POSITIONCENTRE','MOVESIDE','ROTATE','SHOOTING','IDLE', 'TIMEOUTKICK'
    STATES = [POSITIONCENTRE, MOVESIDE, ROTATE, SHOOTING, IDLE, TIMEOUTKICK]

    UP, DOWN = 'UP', 'DOWN'
    GOAL_SIDES = [UP, DOWN]

    def __init__(self, world, communications):
        super(AttackerShootYordan, self).__init__(world, self.STATES,
                communications)

        self.NEXT_ACTION_MAP = {self.POSITIONCENTRE : self.positionCentre,
                                self.MOVESIDE: self.moveSide,
                                self.ROTATE: self.rotate,
                                self.SHOOTING : self.shoot,
                                self.IDLE : self.idle,
                                self.TIMEOUTKICK : self.timeoutKick}

        self.our_attacker = self.world.our_attacker
        self.their_defender = self.world.their_defender
        self.their_goal = self.world.their_goal
        self.zone = 2 if self.world._our_side == 'left' else 1
        box = self.world.pitch.zones[self.zone].boundingBox()
        self.zone_box = {
                "min": Vector2(box[0], box[2]),
                "max": Vector2(box[1], box[3])}
        self.timer = 0
        self.last_offset = 0

    def positionCentre(self):
        zone_center = (self.zone_box["max"] + self.zone_box["min"]) * 0.5
        desired_position = zone_center
        displacement = self.our_attacker.current.position - desired_position

        if displacement.len() < 5.0 * CM_TO_PIXEL:
            linear_power = Vector2(0.0,0.0)
        else:
            linear_power = self.go_to_position(desired_position, travel_power = 0.6,
                    precision_power = 0.3)

        ang_power = self.rotate_to_angle(0, travel_power = 0.0, precision_power = 0.0)

        command = MoveCommand(power = linear_power,
                            angle = self.our_attacker.angle,
                            rotation_power = ang_power)

        self.communications.put_command(command)

        if (displacement.len() < 5.0 * CM_TO_PIXEL):
            self.communications.put_command(StopCommand())
            self.timer = datetime.now()
            self.current_state = self.ROTATE

    def moveSide(self):

        if(self.zone == 2):
          if(self.our_attacker.current.angle > 0.0 and self.our_attacker.current.angle < math.pi):
            desired_angle = 0.0
          else:
            desired_angle = 2 * math.pi
        else:
          desired_angle = math.pi

        offset_from_desired_angle = math.fabs((self.our_attacker.current.angle - desired_angle))
        offset_from_desired_defender = math.fabs(self.our_attacker.current.position.y - self.their_defender.current.position.y)

        if LOG_STRT > 1:
          print "DESIRED ANGLE", desired_angle
          print "OFFSET ANGLE", offset_from_desired_angle, 'ATTACKER ANGLE', self.our_attacker.current.angle
          print "OFFSET FROM DEFENDER", offset_from_desired_defender / CM_TO_PIXEL

        if(offset_from_desired_defender >= 17 * CM_TO_PIXEL and offset_from_desired_angle < 0.1):
          self.communications.put_command(StopCommand())
          self.current_state = self.SHOOTING

        zone_center = (self.zone_box["max"] + self.zone_box["min"]) * 0.5
        moveOffset = 0
        shootOffset = 0
        if self.their_defender.y < zone_center.y:
            side = self.UP
            moveOffset = 50
            shootOffset = 60
        else:
            side = self.DOWN
            moveOffset = -50
            shootOffset = -60

        desired_position = Vector2(zone_center.x, zone_center.y + moveOffset)
        displacement = self.our_attacker.current.position - desired_position

        if displacement.len() < 5.0 * CM_TO_PIXEL:
            linear_power = Vector2(0,0)
        else:
            linear_power = self.go_to_position(desired_position, travel_power = 0.6,
                    precision_power = 0.3)

        ang_power = self.rotate_to_angle(desired_angle, travel_power = 0.0, precision_power = 0.0)
        offset_from_desired_angle = (self.our_attacker.current.angle - desired_angle) % (math.pi * 2)

        if offset_from_desired_angle > math.pi:
            offset_from_desired_angle -= math.pi * 2

        command = MoveCommand(power = linear_power,
                            angle = self.our_attacker.angle,
                            rotation_power = ang_power)

        self.communications.put_command(command)

        if (datetime.now()-self.timer > timedelta(seconds=12)):
            self.current_state = self.TIMEOUTKICK

    def rotate(self):

        if(self.zone == 2):
          if(self.our_attacker.current.angle > 0.0 and self.our_attacker.current.angle < math.pi):
            desired_angle = 0.0
          else:
            desired_angle = 2 * math.pi
        else:
          desired_angle = math.pi

        ang_power = self.rotate_to_angle(desired_angle, travel_power = 0.4, precision_power = 0.3)
        offset_from_desired_angle = (self.our_attacker.current.angle - desired_angle) % (math.pi * 2)

        if offset_from_desired_angle > math.pi:
            offset_from_desired_angle -= math.pi * 2

        if LOG_STRT > 1:
            #print 'shoot offset: ', shootOffset
            print 'angle offset: ', offset_from_desired_angle

        command = MoveCommand(power = Vector2(0.0,0.0),
                            angle = self.our_attacker.angle,
                            rotation_power = ang_power)

        self.communications.put_command(command)

        if (math.fabs(offset_from_desired_angle) < 0.1):
            self.current_state = self.MOVESIDE

    def timeoutKick(self):

        zone_center = (self.zone_box["max"] + self.zone_box["min"]) * 0.5
        moveOffset = 60
        shootOffset = -60

        desired_position = Vector2(zone_center.x,zone_center.y + moveOffset)
        displacement = self.our_attacker.current.position - desired_position

        if displacement.len() < 5.0 * CM_TO_PIXEL:
            linear_power = Vector2(0.0,0.0)
        else:
            linear_power = self.go_to_position(desired_position, travel_power = 0.6,
                    precision_power = 0.3)

        if(self.zone == 2):
          if(self.our_attacker.current.angle > 0.0 and self.our_attacker.current.angle < math.pi):
            desired_angle = 0.0
          else:
            desired_angle = 2 * math.pi
        else:
          desired_angle = math.pi

        ang_power = self.rotate_to_angle(desired_angle, travel_power = 0.3, precision_power = 0.3)
        offset_from_desired_angle = (self.our_attacker.current.angle - desired_angle) % (math.pi * 2)

        if offset_from_desired_angle > math.pi:
            offset_from_desired_angle -= math.pi * 2

        command = MoveCommand(power = linear_power,
                            angle = self.our_attacker.angle,
                            rotation_power = ang_power)

        self.communications.put_command(command)

        if (displacement.len() < 5.0 * CM_TO_PIXEL and math.fabs(offset_from_desired_angle) < 0.1):
            self.communications.put_command(StopCommand())
            self.current_state = self.SHOOTING

    def shoot(self):
        self.communications.put_command(KickCommand())
        self.current_state = self.IDLE


class AttackerShootSlideFast(Strategy):

    POSITIONCENTRE, MOVESIDE, ROTATE, SHOOTING, IDLE, TIMEOUTKICK = 'POSITIONCENTRE','MOVESIDE','ROTATE','SHOOTING','IDLE', 'TIMEOUTKICK'
    STATES = [POSITIONCENTRE, MOVESIDE, ROTATE, SHOOTING, IDLE, TIMEOUTKICK]

    UP, DOWN = 'UP', 'DOWN'
    GOAL_SIDES = [UP, DOWN]
    NEAR, FAR = 'NEARPOST', 'FARPOST'
    GOAL_SIDES = [UP, DOWN]

    def __init__(self, world, communications):
        super(AttackerShootSlideFast, self).__init__(world, self.STATES,
                communications)

        self.NEXT_ACTION_MAP = {self.POSITIONCENTRE : self.positionCentre,
                                self.MOVESIDE: self.moveSide,
                                self.ROTATE: self.rotate,
                                self.SHOOTING : self.shoot,
                                self.IDLE : self.idle,
                                self.TIMEOUTKICK : self.timeoutKick}

        self.our_attacker = self.world.our_attacker
        self.their_defender = self.world.their_defender
        self.their_goal = self.world.their_goal
        self.zone = 2 if self.world._our_side == 'left' else 1
        box = self.world.pitch.zones[self.zone].boundingBox()
        self.zone_box = {
                "min": Vector2(box[0], box[2]),
                "max": Vector2(box[1], box[3])}
        self.timer = 0
        self.last_offset = 0

    def fast_shoot(self):
        ''' If goal clear, shoot immediately '''
        our_side = self.world._our_side
        attx = self.our_attacker.current.position.x
        atty = self.our_attacker.current.position.y
        attv = self.our_attacker.current.velocity.y
        dfnx = self.their_defender.current.position.x
        dfny = self.their_defender.current.position.y
        dfnv = self.their_defender.current.velocity.y
        alpha = self.our_attacker.current.angle if our_side == 'left' \
            else pi - self.our_attacker.current.angle
        if 0.3 * pi < alpha < 1.7 * pi:
            return False
        # clearance between the two robots
        clearance = math.fabs(atty - dfny)
        # clearance between the two robots after half a second if our stops
        clearance_fut = math.fabs(atty - dfny - dfnv / 1.0)
        if clearance < 7.0 * CM_TO_PIXEL or \
            clearance_fut < 6.0 * CM_TO_PIXEL:
            #return False
            pass
        sights = atty - (self.their_goal.x - attx) * math.tan(alpha)
        sights_fut = atty + attv / 1.0 - \
                (self.their_goal.x - attx) * math.tan(alpha)
        if not(self.their_goal.y - 30 < sights < self.their_goal.y + 30):
            return False
        if not(self.their_goal.y - 35 < sights_fut < self.their_goal.y + 35):
            return False
        sights_def = atty - (dfnx - attx) * math.tan(alpha)
        sights_def_fut = atty + attv/1.0 - (dfnx - attx) * math.tan(alpha)
        if math.fabs(sights_def - dfny) < 7.0 * CM_TO_PIXEL:
            return False
        if math.fabs(sights_def - dfny - dfnv) < 6.5 * CM_TO_PIXEL:
            return False
        if math.fabs(sights_def_fut - dfny) < 6.5 * CM_TO_PIXEL:
            return False
        if math.fabs(sights_def_fut - dfny - dfnv) < 6.5 * CM_TO_PIXEL:
            return False
        if LOG_STRT > 0:
            print 'shoot fast'
        self.communications.put_command(StopCommand())
        self.current_state = self.SHOOTING
        return True

    def positionCentre(self):
        if self.fast_shoot():
            return
        zone_center = (self.zone_box["max"] + self.zone_box["min"]) * 0.5
        desired_position = Vector2(zone_center.x, self.our_attacker.current.position.y)
        displacement = self.our_attacker.current.position - desired_position

        if displacement.len() < 10.0 * CM_TO_PIXEL:
            linear_power = Vector2(0,0)
        else:
            linear_power = self.go_to_position(desired_position, travel_power = 0.6,
                    precision_power = 0.3)

        shooting_line = Vector2(self.their_goal.x,self.their_goal.y) - self.our_attacker.current.position
        desired_angle = shooting_line.angle()
        ang_power = self.rotate_to_angle(desired_angle, travel_power = 0.4, precision_power = 0.3)
        offset_from_desired_angle = (self.our_attacker.current.angle - desired_angle) % (math.pi * 2)

        if offset_from_desired_angle > math.pi:
            offset_from_desired_angle -= math.pi * 2

        command = MoveCommand(power = linear_power,
                            angle = self.our_attacker.angle,
                            rotation_power = ang_power)

        self.communications.put_command(command)

        if (displacement.len() < 10.0 * CM_TO_PIXEL and math.fabs(offset_from_desired_angle) < 0.25):
            self.communications.put_command(StopCommand())
            self.timer = datetime.now()
            self.current_state = self.MOVESIDE

    def moveSide(self):
        if self.fast_shoot():
            return
        zone_center = (self.zone_box["max"] + self.zone_box["min"]) * 0.5
        #moveOffset = 0
        #shootOffset = 0
        if self.their_defender.y < zone_center.y:
            side = self.UP
            moveOffset = 80
            shootOffset = 60
        else:
            side = self.DOWN
            moveOffset = -80
            shootOffset = -60
        if (self.world._our_side == 'left' and side == self.UP) or \
           (self.world._our_side == 'right' and side == self.DOWN):
            corner = self.NEAR
        else:
            corner = self.FAR

        desired_position = Vector2(zone_center.x,self.their_goal.y+moveOffset)
        displacement = self.our_attacker.current.position - desired_position

        if displacement.len() < 10.0 * CM_TO_PIXEL:
            linear_power = Vector2(0,0)
        else:
            linear_power = self.go_to_position(desired_position, travel_power = 0.8,
                    precision_power = 0.3)

        shooting_line = Vector2(self.their_goal.x,self.their_goal.y+shootOffset) - self.our_attacker.current.position
        desired_angle = shooting_line.angle()
        ang_power = self.rotate_to_angle(desired_angle, travel_power = 0.6, precision_power = 0.4)
        offset_from_desired_angle = (self.our_attacker.current.angle - desired_angle) % (math.pi * 2)

        if offset_from_desired_angle > math.pi:
            offset_from_desired_angle -= math.pi * 2

        command = MoveCommand(power = linear_power,
                            angle = self.our_attacker.angle,
                            rotation_power = ang_power)

        self.communications.put_command(command)

        if (datetime.now()-self.timer > timedelta(seconds=12)):
            self.current_state = self.TIMEOUTKICK

        if corner == self.NEAR:
            angle_threshold = 0.15
        else:
            angle_threshold = 0.09

        if (displacement.len() < 10.0 * CM_TO_PIXEL and math.fabs(offset_from_desired_angle) < angle_threshold):
            if (side == self.UP and self.their_defender.y < zone_center.y):
                self.communications.put_command(StopCommand())
                self.current_state = self.ROTATE
            elif (side == self.DOWN and self.their_defender.y > zone_center.y):
                self.communications.put_command(StopCommand())
                self.current_state = self.ROTATE

    def rotate(self):
        if self.fast_shoot():
            return
        zone_center = (self.zone_box["max"] + self.zone_box["min"]) * 0.5
        shootOffset = 0
        if self.their_defender.y < zone_center.y:
            shootOffset = 50
        else:
            shootOffset = -50

        shooting_line = Vector2(self.their_goal.x,self.their_goal.y+shootOffset) - self.our_attacker.current.position
        desired_angle = shooting_line.angle()
        ang_power = self.rotate_to_angle(desired_angle, travel_power = 0.6, precision_power = 0.3)
        offset_from_desired_angle = (self.our_attacker.current.angle - desired_angle) % (math.pi * 2)

        if offset_from_desired_angle > math.pi:
            offset_from_desired_angle -= math.pi * 2

        if LOG_STRT > 1:
            print 'shoot offset: ', shootOffset
            print 'angle offset: ', offset_from_desired_angle

        command = MoveCommand(power = Vector2(0,0),
                            angle = self.our_attacker.angle,
                            rotation_power = ang_power)

        self.communications.put_command(command)

        if (math.fabs(offset_from_desired_angle) < 0.1):
            if (shootOffset > 0 and self.last_offset > 0.0):
                self.current_state = self.SHOOTING
            elif (shootOffset < 0 and self.last_offset < 0.0):
                self.current_state = self.SHOOTING
        self.last_offset = offset_from_desired_angle

    def timeoutKick(self):
        if self.fast_shoot():
            return
        zone_center = (self.zone_box["max"] + self.zone_box["min"]) * 0.5
        moveOffset = 80
        shootOffset = -60

        desired_position = Vector2(zone_center.x,self.their_goal.y+moveOffset)
        displacement = self.our_attacker.current.position - desired_position

        if displacement.len() < 10.0 * CM_TO_PIXEL:
            linear_power = Vector2(0,0)
        else:
            linear_power = self.go_to_position(desired_position, travel_power = 0.6,
                    precision_power = 0.3)

        shooting_line = Vector2(self.their_goal.x,self.their_goal.y+shootOffset) - self.our_attacker.current.position
        desired_angle = shooting_line.angle()
        ang_power = self.rotate_to_angle(desired_angle, travel_power = 0.6, precision_power = 0.3)
        offset_from_desired_angle = (self.our_attacker.current.angle - desired_angle) % (math.pi * 2)

        if offset_from_desired_angle > math.pi:
            offset_from_desired_angle -= math.pi * 2

        command = MoveCommand(power = linear_power,
                            angle = self.our_attacker.angle,
                            rotation_power = ang_power)

        self.communications.put_command(command)

        if (displacement.len() < 10.0 * CM_TO_PIXEL and math.fabs(offset_from_desired_angle) < 0.09):
            self.communications.put_command(StopCommand())
            self.current_state = self.SHOOTING

    def shoot(self):
        self.communications.put_command(KickCommand())
        self.current_state = self.IDLE


class AttackerShootWait(Strategy):

    POSITIONCENTRE, MOVESIDE, ROTATE, SHOOTING, IDLE = 'POSITIONCENTRE','MOVESIDE','ROTATE','SHOOTING','IDLE'
    STATES = [POSITIONCENTRE, MOVESIDE, ROTATE, SHOOTING, IDLE]

    UP, DOWN = 'UP', 'DOWN'
    GOAL_SIDES = [UP, DOWN]

    def __init__(self, world, communications):
        super(AttackerShootWait, self).__init__(world, self.STATES,
                communications)

        self.NEXT_ACTION_MAP = {self.POSITIONCENTRE : self.positionCentre,
                                self.MOVESIDE: self.moveSide,
                                self.ROTATE: self.rotate,
                                self.SHOOTING : self.shoot,
                                self.IDLE : self.idle}

        self.our_attacker = self.world.our_attacker
        self.their_defender = self.world.their_defender
        self.their_goal = self.world.their_goal
        self.zone = 2 if self.world._our_side == 'left' else 1
        box = self.world.pitch.zones[self.zone].boundingBox()
        self.zone_box = {
                "min": Vector2(box[0], box[2]),
                "max": Vector2(box[1], box[3])}
        self.start_time = 0

    def positionCentre(self):
        zone_center = (self.zone_box["max"] + self.zone_box["min"]) * 0.5
        desired_position = zone_center
        displacement = self.our_attacker.current.position - desired_position

        if displacement.len() < 10.0 * CM_TO_PIXEL:
            linear_power = Vector2(0,0)
        else:
            linear_power = self.go_to_position(desired_position, travel_power = 0.6,
                    precision_power = 0.3)

        shooting_line = Vector2(self.their_goal.x,self.their_goal.y) - self.our_attacker.current.position
        desired_angle = shooting_line.angle()
        ang_power = self.rotate_to_angle(desired_angle, travel_power = 0.6, precision_power = 0.3)
        offset_from_desired_angle = (self.our_attacker.current.angle - desired_angle) % (math.pi * 2)

        if offset_from_desired_angle > math.pi:
            offset_from_desired_angle -= math.pi * 2

        command = MoveCommand(power = linear_power,
                            angle = self.our_attacker.angle,
                            rotation_power = ang_power)

        self.communications.put_command(command)

        if (displacement.len() < 10.0 * CM_TO_PIXEL and math.fabs(offset_from_desired_angle) < 0.25):
            self.communications.put_command(StopCommand())
            self.start_time = datetime.now()
            self.current_state = self.MOVESIDE

    def moveSide(self):

        zone_center = (self.zone_box["max"] + self.zone_box["min"]) * 0.5
        moveOffset = 0
        aimOffset = 0
        waitOffset = 0
        if self.their_defender.y < zone_center.y:
            side = self.UP
            moveOffset = 50
            aimOffset = -200
            waitOffset = -40
        else:
            side = self.DOWN
            moveOffset = -50
            aimOffset = 200
            waitOffset = 40

        desired_position = Vector2(zone_center.x,self.their_goal.y+moveOffset)
        displacement = self.our_attacker.current.position - desired_position

        if displacement.len() < 10.0 * CM_TO_PIXEL:
            linear_power = Vector2(0,0)
        else:
            linear_power = self.go_to_position(desired_position, travel_power = 0.6,
                    precision_power = 0.3)

        shooting_line = Vector2(self.their_goal.x,self.their_goal.y+aimOffset) - self.our_attacker.current.position
        desired_angle = shooting_line.angle()
        ang_power = self.rotate_to_angle(desired_angle, travel_power = 0.6, precision_power = 0.3)
        offset_from_desired_angle = (self.our_attacker.current.angle - desired_angle) % (math.pi * 2)

        if offset_from_desired_angle > math.pi:
            offset_from_desired_angle -= math.pi * 2

        command = MoveCommand(power = linear_power,
                            angle = self.our_attacker.angle,
                            rotation_power = ang_power)

        self.communications.put_command(command)

        if (displacement.len() < 10.0*CM_TO_PIXEL and math.fabs(offset_from_desired_angle) < 0.2):
            self.communications.put_command(StopCommand())
            move = datetime.now()-self.start_time>timedelta(seconds=6)
            if (side == self.UP and self.their_defender.current.position.y < self.their_goal.y+waitOffset or move):
                self.current_state = self.ROTATE
            elif (side == self.DOWN and (self.their_defender.current.position.y > self.their_goal.y+waitOffset) or move):
                self.current_state = self.ROTATE

    def rotate(self):

        zone_center = (self.zone_box["max"] + self.zone_box["min"]) * 0.5
        moveOffset = 0
        if self.their_defender.y < zone_center.y:
            side = self.UP
            moveOffset = 60
        else:
            side = self.DOWN
            moveOffset = -60

        shooting_line = Vector2(self.their_goal.x,self.their_goal.y+moveOffset) - self.our_attacker.current.position
        desired_angle = shooting_line.angle()
        ang_power = self.rotate_to_angle(desired_angle, travel_power = 0.6, precision_power = 0.3)
        offset_from_desired_angle = (self.our_attacker.current.angle - desired_angle) % (math.pi * 2)

        if offset_from_desired_angle > math.pi:
            offset_from_desired_angle -= math.pi * 2

        command = MoveCommand(power = Vector2(0,0),
                            angle = self.our_attacker.angle,
                            rotation_power = ang_power)

        self.communications.put_command(command)

        if LOG_STRT > 1:
            print math.fabs(offset_from_desired_angle)

        if (math.fabs(offset_from_desired_angle) < GOAL_ANGLE_THRES):
            self.communications.put_command(StopCommand())
            self.current_state = self.SHOOTING

    def shoot(self):
        self.communications.put_command(KickCommand())
        self.current_state = self.IDLE


