from communications.vector2 import Vector2
from models import *
from collisions import *
from strategies import *
from utilities import *

LOG_PLAN = 0

# Global variables
PIXEL_TO_CM = 115.2 / 280.0
CM_TO_PIXEL = 1.0 / PIXEL_TO_CM
GRAB_RANGE = 20 * CM_TO_PIXEL
LOSE_RANGE = 40 * CM_TO_PIXEL


class Planner:
    '''
    Takes the state of the world and chooses a strategy
    '''
    def __init__(self, our_side, pitch_num, communications, sender, receiver):
        self._world = World(our_side, pitch_num)
        self.communications = communications
        self.sender   = sender
        self.receiver = receiver
        self.our_defender   = self._world.our_defender
        self.our_attacker   = self._world.our_attacker
        self.their_defender = self._world.their_defender
        self.their_attacker = self._world.their_attacker
        self.ball           = self._world.ball
        self.goal           = self._world.their_goal

        # construct the zone for carefull grabbing which is the same as the zone box
        # but with a 10cm margin on the left and right sides. If the ball is in the zone
        # but outside of the zone_carefull_grab then it is near the zone edge and we grab carefully
        self.zone = 2 if self._world._our_side == 'left' else 1
        box = self._world.pitch.zones[self.zone].boundingBox()
        self.zone_box = {
                "min": Vector2(box[0], box[2]),
                "max": Vector2(box[1], box[3])}

        back_left   = (self.zone_box["min"].x + 10 * CM_TO_PIXEL,  self.zone_box["min"].y)
        back_right  = (self.zone_box["min"].x + 10 * CM_TO_PIXEL,  self.zone_box["max"].y)
        front_left  = (self.zone_box["max"].x - 10 * CM_TO_PIXEL, self.zone_box["min"].y)
        front_right = (self.zone_box["max"].x - 10 * CM_TO_PIXEL, self.zone_box["max"].y)
        self.zone_carefull_grab = Polygon((front_left, front_right, back_right, back_left))

        # signals transition from normal grab to grab carefully
        self.grab_index = 0
        self.last_grab_index = 0

        self.shoot_index = 3

        # Ball tracking finite state machine
        self._bcurr_state = {
            'owned'   : False,
            'zone'    : self.our_attacker.zone}

        self._blast_state = self._bcurr_state
        self._world.ball_state = self._bcurr_state

        self._attacker_strategies = {
            'defend'   : [AttackerDefend],
            'grab'     : [AttackerGrab, AttackerGrabCarefully],
            'shoot'    : [AttackerShoot, AttackerShootYordan, AttackerShootSlide, AttackerShootSlideFast],
            'catch'    : [AttackerPosition],
            'intercept': [AttackerIntercept]}

        self.attacker_state = 'defend'
        self.attacker_current_strategy = self.choose_attacker_strategy(
                self._world, 0)
        self.attacker_current_strategy.generate()

        # Start communications - comment this to not run them
        self.sender.start()
        self.receiver.start()


    def update_ball_state(self):
        ''' Update ball FSM state'''
        # Ball position
        self.update_ball_zone()
        if LOG_PLAN > 1:
            print 'ball zone: ', self._bcurr_state['zone']
        self.update_ball_own(self.our_attacker)
        self._blast_state = self._bcurr_state

    def update_ball_zone(self):
        ''' Update ball zone '''
        if not self.ball.current.visible:
            return
        for i, zone in enumerate(self._world.pitch.zones):
            if zone.isInside(self.ball.current.position.x, \
                    self.ball.current.position.y):
                self._bcurr_state['zone'] = i
                return

    def update_ball_own(self, robot):
        vis = self.ball.current.visible
        own = self._blast_state['owned']

        # If we can not see the ball take the last own state
        self._bcurr_state['owned'] = own

        # we see the ball.
        # - if we had it in the last state we check if it is still in the buffer
        # area and if not - we don't have it anymore
        # - if we did not have it - check if it is in the cather area and if yes
        # then we have it
        if vis and self._blast_state['owned']:
            #back_left   = (-0.5 * CM_TO_PIXEL,  -3.25 * CM_TO_PIXEL)
            #back_right  = (-0.5 * CM_TO_PIXEL,   3.25 * CM_TO_PIXEL)
            #front_left  = (4    * CM_TO_PIXEL,  -9.5 * CM_TO_PIXEL)
            #front_right = (4    * CM_TO_PIXEL,   9.5 * CM_TO_PIXEL)
            back_left   = (-2 * CM_TO_PIXEL,  -5 * CM_TO_PIXEL)
            back_right  = (-2 * CM_TO_PIXEL,   5 * CM_TO_PIXEL)
            front_left  = (5    * CM_TO_PIXEL,  -11 * CM_TO_PIXEL)
            front_right = (5    * CM_TO_PIXEL,   11 * CM_TO_PIXEL)
            buffer = Polygon((front_left, front_right, back_right, back_left))
            buffer.shift(robot.current.position.x + robot._catcher_area['front_offset'], robot.current.position.y)
            buffer.rotate(robot.current.angle, robot.current.position.x, robot.current.position.y)


            if not buffer.isInside(self.ball.current.position.x, self.ball.current.position.y):
                self._bcurr_state['owned'] = False

        elif not self._blast_state['owned']:
            back_left   = (0,                 -2.75 * CM_TO_PIXEL)
            back_right  = (0,                  2.75 * CM_TO_PIXEL)
            front_left  = (3.5 * CM_TO_PIXEL, -9.0 * CM_TO_PIXEL)
            front_right = (3.5 * CM_TO_PIXEL,  9.0 * CM_TO_PIXEL)
            catcher = Polygon((front_left, front_right, back_right, back_left))
            catcher.shift(robot.current.position.x + robot._catcher_area['front_offset'], robot.current.position.y)
            catcher.rotate(robot.current.angle, robot.current.position.x, robot.current.position.y)

            if catcher.isInside(self.ball.current.position.x, self.ball.current.position.y):
                self._bcurr_state['owned'] = True


    # Choose the first strategy in the applicable list.
    def choose_attacker_strategy(self, world, index):
        next_strategy = self._attacker_strategies[self.attacker_state][index]
        return next_strategy(world, self.communications)

    # Get Attacker Current Strategy
    @property
    def attacker_current_strategy(self):
        return self._attacker_current_strategy

    # Set Attacker Current Strategy
    @attacker_current_strategy.setter
    def attacker_current_strategy(self, new_strategy):
        self._attacker_current_strategy = new_strategy

    # Get Attacker Strategy FSM state (inner)
    @property
    def attacker_strat_state(self):
        return self.attacker_current_strategy.current_state

    # Get Attacker Strategy FSM state (outer)
    @property
    def attacker_state(self):
        return self._attacker_state

    # Set Attacker Strategy FSM state (outer)
    @attacker_state.setter
    def attacker_state(self, new_state):
        assert new_state in ['defend', 'grab', 'shoot', 'catch', 'intercept']
        self._attacker_state = new_state

    # Update world state
    def update_world(self, position_dictionary, current_state):
        self._world.update_positions(position_dictionary, current_state)

    # Decide what to do and return actions
    def plan(self):
        # receive message from our allies
        ally_mess = self.receiver.data

        self.update_ball_state()

        # If ball is in our zone and visible, then grab the ball and shoot:
        if self._bcurr_state['zone'] == self.our_attacker.zone:

            self.last_grab_index = self.grab_index
            if self.zone_carefull_grab.isInside(self.ball.current.position.x, self.ball.current.position.y):
              self.grab_index = 0
            else:
              self.grab_index = 1

            # if we are grabbing and have already grabbed then shoot
            # if we try to shoot but do not have the ball, go back to
            # grabbing and try again
            if self.attacker_state == 'shoot' and \
                      not self._bcurr_state['owned']:
                self.attacker_state = 'grab'
                self.attacker_current_strategy = \
                  self.choose_attacker_strategy(self._world, self.grab_index)

            # if we are shooting and the ball is still in us continue shooting
            elif self.attacker_state == 'shoot':
                pass

            elif self.attacker_state == 'grab' and \
                  self.attacker_strat_state == 'GRABBED':
                self.attacker_state = 'shoot'
                self.attacker_current_strategy = \
                    self.choose_attacker_strategy(self._world, self.shoot_index)

            # if we are grabbing but the ball is still not grabbed continue
            # grabbing
            elif self.attacker_state == 'grab' and self.last_grab_index == self.grab_index:
                pass

            # if we are still grabbing but we have to switch between grabbing strategies
            elif self.attacker_state == 'grab' and self.last_grab_index != self.grab_index:
                self.attacker_current_strategy = \
                self.choose_attacker_strategy(self._world, self.grab_index)
                self.attacker_current_strategy.current_state = self.attacker_current_strategy.GO_TO_BALL

            # if we have been defending, intercepting or catching we go into grab
            # but wihtout opennign the grabber again in order not to kick the ball
            # accidentally
            elif self.attacker_state in ['defend', 'catch', 'intercept']:
                self.attacker_state = 'grab'
                self.attacker_current_strategy = \
                    self.choose_attacker_strategy(self._world, self.grab_index)
                self.attacker_current_strategy.current_state = self.attacker_current_strategy.GO_TO_BALL

            self.attacker_current_strategy.generate()

        # If the ball is in our defender zone, prepare to catch the passed
        # ball:
        elif self._bcurr_state['zone'] == self.our_defender.zone:
            if not self.attacker_state == 'catch':
                self.attacker_state = 'catch'
                self.attacker_current_strategy = \
                    self.choose_attacker_strategy(self._world, 0)
            self.attacker_current_strategy.generate()

        # If the ball is in their defender zone, defend
        elif self._bcurr_state['zone'] == self.their_defender.zone:
            if not self.attacker_state == 'defend':
                self.attacker_state = 'defend'
                self.attacker_current_strategy = \
                    self.choose_attacker_strategy(self._world, 0)
            self.attacker_current_strategy.generate()
        elif self._bcurr_state['zone'] == self.their_attacker.zone:
            self.attacker_state = 'intercept'
            self.attacker_current_strategy = \
                    self.choose_attacker_strategy(self._world, 0)
            self.attacker_current_strategy.generate()

        # send message to our allies
        self.sender.data = 'message'
