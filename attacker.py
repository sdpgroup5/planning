__author__ = 'Allan'

# Patch to change working directory to parent directory of the planning directory.
import os
import sys
path = os.path.abspath(os.path.join(os.path.dirname(__file__), '..'))
if not path in sys.path:
    sys.path.insert(1,path)
del path

from postprocessing.postprocessing import *
from vision.vision import *
from planning.models import *
from communications.controller import *
from communications.vector2 import *
import math
import time
from datetime import datetime

PIXEL_TO_CM = 115.2 / 280.0
CM_TO_PIXEL = 1.0 / PIXEL_TO_CM
GRAB_RANGE = 20 * CM_TO_PIXEL
LOSE_RANGE = 40 * CM_TO_PIXEL

class Attacker(object):
    """ Strategy building blocks required for an attacking robot """
    def __init__(self, preprocessing, postprocessing, vision, get_frame_callback, communications, our_side, pitch_number, zone):
        self.vision = vision
        self.preprocessing = preprocessing
        self.postprocessing = postprocessing
        self.get_frame_callback = get_frame_callback
        self.communications = communications
        frame = self.get_frame_callback()
        while (frame is None):
            frame = self.get_frame_callback()
        self.vectors = self.postprocessing.analyze(self.vision.locate(frame)[0])
        self.world = World(our_side, pitch_number)
        self.world.update_positions(self.vectors)

        self.zone = zone
        if (our_side == "left"):
            self.robot = [
                self.world.our_defender,
                self.world.their_attacker,
                self.world.our_attacker,
                self.world.their_defender
            ][zone]
        else:
            self.robot = [
                self.world.their_defender,
                self.world.our_attacker,
                self.world.their_attacker,
                self.world.our_defender
            ][zone]
        # self.robot = self.world.our_attacker
        self.robot.catcher_area = {"width" : 6, "height" : 17, "front_offset" : 7}

        box = self.world.pitch.zones[self.zone].boundingBox()
        self.zone_box = {"min": Vector2(box[0], box[2]), "max": Vector2(box[1], box[3])}

        self.ball = self.world.ball
        self.goal = self.world.their_goal

        self.last_state = {"ball_visible": False, "ball_owned": False}

    def updatepositions(self):
        """ Updates the world state for object.

        Uses the object's camera and other parameters to get the positions of real world items relevant to the attacker.
        """
        frame = self.get_frame_callback()
        while (frame is None):
            frame = self.get_frame_callback()
        preprocessed = self.preprocessing.run(frame, self.preprocessing.options)['frame']
        located = self.vision.locate(preprocessed)[0]
        self.vectors = self.postprocessing.analyze(located)
        self.world.update_positions(self.vectors)

    def do_logic(self):
        cur_state = {}
        bot = self.robot
        ball = self.world.ball

        # Ball position
        v_ball_pos = Vector2(ball.x, ball.y)
        # Robot position
        v_bot_pos = Vector2(bot.x, bot.y)
        # Vector from robot to ball
        v_bot_to_ball = v_ball_pos - v_bot_pos
        zone_center = (self.zone_box["max"] + self.zone_box["min"]) * 0.5

        # --- DETERMINE IDEAL ANGLE ---
        ideal_angle = 0
        if (self.last_state['ball_owned']):
            ideal_angle = math.pi - self.goal.angle
        else:
            ideal_angle = v_bot_to_ball.angle()

        # Offset of robot from ideal_angle [-pi, pi]
        angle_offset = (self.robot.angle - ideal_angle) % (math.pi * 2)
        if (angle_offset > math.pi):
            angle_offset -= math.pi * 2

        # --- DETERMINE IDEAL POSITION ---
        ideal_position = zone_center
        grab_cone_width = math.pi / 3.5
        lose_cone_width = math.pi / 2.0
        in_grab_cone = -grab_cone_width / 2 < angle_offset < grab_cone_width / 2

        if (self.last_state['ball_owned']):
            min = self.zone_box["min"]
            max = self.zone_box["max"]
            ideal_position = zone_center +
                    Vector2.from_angle(math.pi - self.goal.angle,
                            (max.x - min.x) * 0.15)
        else:
            # Ideal distance in pixels
            equilibrium_distance = 20 * CM_TO_PIXEL
            if (in_grab_cone):
                ideal_position = v_ball_pos
            else:
                ideal_position = v_ball_pos + (v_bot_to_ball.norm() * -equilibrium_distance)

        # --- TURNING ---

        # Turn speed relative to angle offset
        if (math.fabs(angle_offset) < math.pi / 7):
            ang_power = 0.11
        elif (math.fabs(angle_offset) < math.pi / 4):
            ang_power = 0.2
        else:
            ang_power = 0.6
        ang_power = math.copysign(ang_power, -angle_offset)

        # --- MOVING ---
        v_bot_to_ideal = ideal_position - v_bot_pos
        # linear_power = v_bot_to_ideal.norm() * (v_bot_to_ideal.len() * 0.1)
        if (self.last_state['ball_owned']):
            linear_power = v_bot_to_ideal * 0.02
        else:
            linear_power = v_bot_to_ideal.norm() * 0.5
        combined_power = linear_power.len() + math.fabs(ang_power)
        print "%0.3f, %0.3f, %0.3f\r" % (ang_power, linear_power.len(), combined_power)
        if (combined_power > 0.6):
            ang_power = ang_power * 0.6 / combined_power
            linear_power = linear_power * (0.6 / combined_power)

        self.communications.set_robot_power({'linear_power' : linear_power, 'angle' : bot.angle, 'ang_power' : ang_power})

        # --- BALL STATE ---

        # Current state ball visible
        is_ball_visible = v_ball_pos.x > 1 or v_ball_pos.y > 1
        # Current state in grab range
        is_ball_in_grab_range = in_grab_cone and v_bot_to_ball.len() < GRAB_RANGE
        # Currently in lose range
        is_ball_in_lose_range = v_bot_to_ball.len() < LOSE_RANGE
        # Last state ball visible
        l_ball_vis = self.last_state['ball_visible']
        # Last state ball owned
        l_ball_own = self.last_state['ball_owned']

        # Kick/grab
        will_grab = False
        if (l_ball_vis and (not l_ball_own) and is_ball_in_grab_range):
            will_grab = True

        # Initialize to same as last state
        cur_state['ball_owned'] = l_ball_own
        cur_state['ball_visible'] = l_ball_vis

        will_kick = False

        # Ball visibility/ownership state machine
        if (is_ball_visible):
            cur_state['ball_visible'] = True
            if (not is_ball_in_lose_range):
                if (l_ball_own):
                    will_kick = True
                cur_state['ball_owned'] = False
        if (will_grab):
            cur_state['ball_owned'] = True
        if (not is_ball_visible):
            cur_state['ball_visible'] = False

        # --- If in position to score goal, KICK ---
        if (l_ball_own and v_bot_to_ideal.len() * PIXEL_TO_CM < 10 and \
                angle_offset < 0.05):
            will_kick = True

        # Handle kicking
        if (will_kick):
            self.communications.kick()
        # Handle grabbing
        if (will_grab):
            self.communications.grab()

        # if (self.last_state['ball_visible'] != cur_state['ball_visible']):
        #     print "visible: %s\r" % str(cur_state['ball_visible'])
        # if (self.last_state['ball_owned'] != cur_state['ball_owned']):
        #     print "owned:   %s\r" % str(cur_state['ball_owned'])

        self.last_state = cur_state
