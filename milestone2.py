#!/afs/inf.ed.ac.uk/user/s12/s1208498/git/sdp/controller/scripts/env/bin/python

from postprocessing.postprocessing import *
from preprocessing.preprocessing import *
from vision.vision import *
from planning.models import *
from communications.controller import *
from communications.vector2 import Vector2
from planning.attacker import *
import math
import threading
import sys
from datetime import datetime, timedelta

# TIME_STEP = 0.2
TIME_STEP = 0.1

class MockComm(object):

    def __init__(self):
        pass

    def stop(self):
        print ("stop\r")

    def grab(self):
        print ("grab\r")

    def kick(self):
        print ("kick\r")

    def set_robot_state(self, state_values, update_timestamp=True):
        print ("move %s %d %d\r") % (state_values['velocity'], state_values['angle'], state_values['ang_vel'])

robot_properties = {"wheel_angles" : {"LEFT" : math.pi, "RIGHT" : 0, "BACK" : 3 * (math.pi / 2)}, "wheel_radii" : {"LEFT" : 7.4, "RIGHT" : 7.4, "BACK" : 7.4}}

# input = raw_input("Enter serial port: ")
# port = "/dev/ttyACM" + input[len(input)-1:len(input)]
port = "/dev/ttyACM0"
pitch = int(raw_input("Enter pitch number: "))
colour = {"b": "blue", "y": "yellow"}[raw_input("Enter colour: ")]
our_side = {"l": "left", "r": "right"}[raw_input("Enter side: ")]

camera = Camera(port=0, pitch=pitch)
frame = camera.get_frame()
frame_shape = frame.shape
frame_centre = camera.get_adjusted_center(frame)

calibration = tools.get_colors(pitch)

# Single char input snippet

class _Getch:
    """Gets a single character from standard input.  Does not echo to the
screen."""
    def __init__(self):
        try:
            self.impl = _GetchWindows()
        except ImportError:
            self.impl = _GetchUnix()

    def __call__(self): return self.impl()


class _GetchUnix:
    def __init__(self):
        import tty, sys

    def __call__(self):
        import sys, tty, termios
        fd = sys.stdin.fileno()
        old_settings = termios.tcgetattr(fd)
        try:
            tty.setraw(sys.stdin.fileno())
            ch = sys.stdin.read(1)
        finally:
            termios.tcsetattr(fd, termios.TCSADRAIN, old_settings)
        return ch


class _GetchWindows:
    def __init__(self):
        import msvcrt

    def __call__(self):
        import msvcrt
        return msvcrt.getch()

getch = _Getch()

# Testing

class CameraThread(threading.Thread):

    def __init__(self, camera):
        threading.Thread.__init__(self)
        self.daemon = True
        self.camera = camera
        self.current_frame = None
        self.frame_lock = threading.Lock()

    def run(self):

        while(True):
            self.current_frame = camera.get_frame()

    def get_current_frame(self):
        frame = self.current_frame
        return frame

class AttackerThread(threading.Thread):

    def __init__(self, c):
        threading.Thread.__init__(self)
        self.daemon = True
        self.c = c

    def run(self):
        self.c.kick()
        while True:
            attacker_active.wait()
            attacker.updatepositions()
            attacker.do_logic()
            time.sleep(TIME_STEP)

camera_thread = CameraThread(camera)
camera_thread.start()

communications = Controller(port, robot_properties, 0.5)
# communications = MockComm()

zone = 2 if our_side == "left" else 1

preprocessing = Preprocessing()
postprocessing = Postprocessing()
vision = Vision(pitch, colour, our_side, frame_shape, frame_centre, calibration)
attacker = Attacker(preprocessing, postprocessing, vision, camera_thread.get_current_frame, communications, our_side, pitch, zone)

attacker_active = threading.Event()
attacker_thread = AttackerThread( communications)
attacker_thread.start()

ERASE_LINE = "\x1b[2K"
ESC = "p"

while(True):
    x = getch()
    if (x == ESC):
        break

    # Toggle attacker logic
    if (x == "a"):
        if (attacker_active.is_set()):
            print "stop\r"
            attacker_active.clear()
            communications.set_robot_state({'velocity': Vector2(0, 0), 'ang_vel': 0})
        else:
            print "run\r"
            attacker_active.set()
