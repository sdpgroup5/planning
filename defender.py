from postprocessing.postprocessing import *
from vision.vision import *
from planning.models import *
from communications.controller import *
from communications.vector2 import *
import math
import time

TIME_STEP = 0.1

class Defender(object):
    def __init__(self,preprocessing, postprocessing,vision,camera,get_frame_callback,communications,side,pitch):
        self.vision = vision
        self.get_frame_callback = get_frame_callback
        self.preprocessing = preprocessing
        self.postprocessing = postprocessing
        self.communications = communications
        rubbish = self.postprocessing.analyze(self.vision.locate(camera.get_frame())[0])
        self.vectors = self.postprocessing.analyze(self.vision.locate(camera.get_frame())[0])
        self.world = World(side, pitch)
        self.world.update_positions(self.vectors)
        self.ball = self.world.ball
        self.robot = self.world.our_defender
        self.goal = self.world.our_goal
        self.side = side
        last_pos = rubbish['our_defender']
        self.last_state = {
            'position': Vector2(last_pos.x, last_pos.y),
            'velocity': Vector2(0, 0),
            "ball_visible": False,
            "ball_owned": False}

    def updatepositions(self):
        frame = self.get_frame_callback()
        while (frame is None):
            frame = self.get_frame_callback()
        preprocessed = self.preprocessing.run(frame, self.preprocessing.options)['frame']
        located = self.vision.locate(preprocessed)[0]
        self.vectors = self.postprocessing.analyze(located)
        self.world.update_positions(self.vectors)
        position = Vector2(self.robot.x, self.robot.y)
        self.last_state['velocity'] = (position - self.last_state['position']) * (1 / TIME_STEP)
        self.last_state['position'] = position

    def gradient(self):
        self.updatepositions()
        x_ball = self.ball.x
        y_ball = self.ball.y
        self.updatepositions()
        x_2 = self.ball.x
        y_2 = self.ball.y
        xdiff = x_2 - x_ball
        grad = 0
        if (xdiff != 0):
            grad = (y_2 - y_ball)/(xdiff)    
        return grad

    def final_y(self):
        #grad = self.gradient()
        #c = y_2 - (grad * x_2)
        #self.updatepositions()
        return self.ball.y

    def vec_test(self):
        print "1"
        self.communications.set_robot_power({'linear_power': Vector2(0,-1), 'angle': 0, 'ang_power': 0})
        print "2"
        time.sleep(1)
        print "3"
        self.communications.set_robot_state({'velocity' : Vector2(0,0), 'angle' : 0, 'ang_vel':0})
        time.sleep(0.1)
        self.communications.set_robot_state({'velocity' : Vector2(0,0), 'angle' : 0, 'ang_vel':0})
        print "4"

    def intercept(self):
        # in_use = False
        final_y = self.final_y()
        angle = math.pi

        # Math
        if self.side == 'right':
            pass
        else:
            angle = 0
        diff = final_y - self.robot.y

        pos = self.last_state['position']
        vel = self.last_state['velocity']
        # Velocity Y component
        vel_y_comp = math.cos(vel.angle_wrt(y_axis)) * vel.len()

        top_speed = 65.625
        brake_power = -vel_y_comp / top_speed * 1.0

        power = 0
        stop_range = 25
        if math.fabs(diff) > stop_range:
            power = math.copysign(1, diff)
        else:
            power = brake_power
        vector = Vector2(0, power)

        # Logic
        move = (self.ball.velocity > 3) and math.fabs(self.robot.y - final_y) >= 15 # 70 and 190
        # move = True

        if move:
            self.communications.set_robot_power({'linear_power': vector, 'angle': angle, 'ang_power': 0})
        else:
            self.communications.set_robot_power({'linear_power': Vector2(0, 0), 'angle': angle, 'ang_power': 0})

        # --- BALL STATE ---

        # Ball position
        cur_state = {}
        bot = self.robot
        ball = self.world.ball

        # Ball position
        v_ball_pos = Vector2(ball.x, ball.y)
        # Robot position
        v_bot_pos = Vector2(bot.x, bot.y)
        # Vector from robot to ball
        v_bot_to_ball = v_ball_pos - v_bot_pos

        # --- DETERMINE IDEAL ANGLE ---
        ideal_angle = 0

        if (self.last_state['ball_owned']):
            ideal_angle = math.pi - self.goal.angle
        else:
            ideal_angle = v_bot_to_ball.angle()

        # Offset of robot from ideal_angle [-pi, pi]
        angle_offset = (self.robot.angle - ideal_angle) % (math.pi * 2)
        if (angle_offset > math.pi):
            angle_offset -= math.pi * 2

        # --- DETERMINE IDEAL POSITION ---
        ideal_position = 0
        grab_cone_width = math.pi / 3.5
        lose_cone_width = math.pi / 2.0
        in_grab_cone = angle_offset > -grab_cone_width / 2 and angle_offset < grab_cone_width / 2

        v_ball_pos = Vector2(ball.x, ball.y)
        # Current state ball visible
        is_ball_visible = v_ball_pos.x > 1 or v_ball_pos.y > 1
        # Current state in grab range
        is_ball_in_grab_range = in_grab_cone and v_bot_to_ball.len() < GRAB_RANGE
        # Currently in lose range
        is_ball_in_lose_range = v_bot_to_ball.len() < LOSE_RANGE
        # Last state ball visible
        l_ball_vis = self.last_state['ball_visible']
        # Last state ball owned
        l_ball_own = self.last_state['ball_owned']

        # Kick/grab
        will_grab = False
        if (l_ball_vis and (not l_ball_own) and is_ball_in_grab_range):
            will_grab = True

        # Initialize to same as last state
        cur_state['ball_owned'] = l_ball_own
        cur_state['ball_visible'] = l_ball_vis

        will_kick = False

        # Ball visibility/ownership state machine
        if (is_ball_visible):
            cur_state['ball_visible'] = True
            if (not is_ball_in_lose_range):
                if (l_ball_own):
                    will_kick = True
                cur_state['ball_owned'] = False
        if (will_grab):
            cur_state['ball_owned'] = True
        if (not is_ball_visible):
            cur_state['ball_visible'] = False

        # --- If has ball, KICK ---
        if l_ball_own:
            will_kick = True

        # Handle kicking
        if (will_kick):
            self.communications.kick()
        # Handle grabbing
        if (will_grab):
            self.communications.grab()

PIXEL_TO_CM = 115.2 / 280.0
CM_TO_PIXEL = 1.0 / PIXEL_TO_CM
GRAB_RANGE = 20 * CM_TO_PIXEL
LOSE_RANGE = 40 * CM_TO_PIXEL